use crate::apu::Apu;
use crate::cartridge::get_mapper;
use crate::cartridge::serialize::MapperData;
use crate::cpu::serialize::CpuData;
use crate::cpu::Cpu;
use crate::input::FuzzingInput;
use crate::ppu::serialize::PpuData;
use crate::ppu::Ppu;
use std::hash::{Hash, Hasher};

#[derive(Clone)]
pub struct FuzzingState {
    pub(crate) cpu_state: CpuData,
    ppu_state: PpuData,
    mapper_state: MapperData,
    pub(crate) frames: usize,
}

impl FuzzingState {
    pub fn save_state(cpu: Cpu, frames: usize) -> FuzzingState {
        FuzzingState {
            cpu_state: cpu.save_state(),
            ppu_state: cpu.ppu.save_state(),
            mapper_state: cpu.mapper.borrow().save_state(),
            frames: frames,
        }
    }

    pub fn default(filename: String) -> FuzzingState {
        let mapper = get_mapper(filename);
        let ppu = Ppu::new(mapper.clone());
        let apu = Apu::new();
        let cpu = Cpu::new(mapper.clone(), ppu, apu);
        return FuzzingState::save_state(cpu, 0);
    }

    pub fn load_state(&self, filename: String) -> (Cpu, usize) {
        let mapper = get_mapper(filename);
        mapper.borrow_mut().load_state(self.mapper_state.clone());
        let mut ppu = Ppu::new(mapper.clone());
        ppu.load_state(self.ppu_state.clone());
        let apu = Apu::new();
        let mut cpu = Cpu::new(mapper.clone(), ppu, apu);
        cpu.load_state(self.cpu_state.clone());
        return (cpu, self.frames);
    }
}

pub struct FuzzingInputState(pub FuzzingInput, pub FuzzingState);

impl Hash for FuzzingInputState {
    fn hash<H: Hasher>(&self, state: &mut H) {
        return self.0.hash(state);
    }
}

impl PartialEq<Self> for FuzzingInputState {
    fn eq(&self, other: &Self) -> bool {
        self.0.eq(&other.0)
    }
}

impl Eq for FuzzingInputState {}
