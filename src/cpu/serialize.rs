use super::Mode;

use serde::{Deserialize, Serialize};
use std::collections::HashSet;

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct CpuData {
    pub(crate) mem: Vec<u8>,
    a: u8,
    x: u8,
    y: u8,
    pub(crate) pc: usize,
    s: u8,
    p: u8,
    clock: u64,
    delay: usize,
    strobe: bool,
    button_states: u8,
    button_states2: u8,
    button_number1: u8,
    button_number2: u8,
    mode_table: Vec<Mode>,
    pub(crate) addresses_fetched: HashSet<usize>,
}

impl super::Cpu {
    pub fn save_state(&self) -> CpuData {
        CpuData {
            mem: self.mem.clone(),
            a: self.a,
            x: self.x,
            y: self.y,
            pc: self.pc,
            s: self.s,
            p: self.p,
            clock: self.clock,
            delay: self.delay,
            strobe: self.strobe,
            button_states: self.button_states,
            button_states2: self.button_states2,
            button_number1: self.button_number1,
            button_number2: self.button_number2,
            mode_table: self.mode_table.clone(),
            addresses_fetched: self.addresses_fetched.clone(),
        }
    }

    pub fn load_state(&mut self, data: CpuData) {
        self.mem = data.mem;
        self.a = data.a;
        self.x = data.x;
        self.y = data.y;
        self.pc = data.pc;
        self.s = data.s;
        self.p = data.p;
        self.clock = data.clock;
        self.delay = data.delay;
        self.strobe = data.strobe;
        self.button_states = data.button_states;
        self.button_states2 = data.button_states2;
        self.button_number1 = data.button_number1;
        self.button_number2 = data.button_number2;
        self.mode_table = data.mode_table;
        self.addresses_fetched = data.addresses_fetched;
    }
}
