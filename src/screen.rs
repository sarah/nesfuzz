use font8x8::legacy::BASIC_LEGACY;

pub struct Screen {
    pub(crate) display: Vec<u32>,
}

impl Screen {
    pub fn new() -> Screen {
        Screen {
            display: vec![0; 256 * 240],
        }
    }

    pub fn plot_pixel(&mut self, ox: usize, oy: usize, r: u32, g: u32, b: u32) {
        let color = (r << 16) + (g << 8) + b;

        let x = ox.clamp(0, 256 - 1);
        let y = oy.clamp(0, 240 - 1);
        self.display[(y * 256) + x] = color
    }

    pub fn draw_string(&mut self, ox: usize, oy: usize, text: &str) {
        let mut xpos = ox;
        let mut ypos = oy;
        for char in text.chars() {
            for x in &BASIC_LEGACY[char as usize] {
                for bit in 0..8 {
                    match *x & 1 << bit {
                        0 => {} //self.plot_pixel(xpos, ypos, 0, 0, 0),
                        _ => self.plot_pixel(xpos, ypos, 0xff, 0xff, 0xff),
                    }
                    xpos += 1
                }
                xpos -= 8;
                ypos += 1;
            }
            xpos += 10;
            ypos -= 8;
        }
    }
}
